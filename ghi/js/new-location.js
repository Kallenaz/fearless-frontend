
//need an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare a variable that will hold the URL for the API view list states function that we just created
    const url = 'http://localhost:8000/api/states/';
    //fetch the URL with the await keyword so that we get the response, not the Promise
    const response = await fetch(url);
    //If the response is okay, get the data using the .json method and await
    if (response.ok) {
      const data = await response.json();
      //console.log(data);
      //Get the select tag element by its id 'state'
      const selectTag = document.getElementById('state');
      //For each state in the states property of the data
      for (let state of data.states) {
        //Create an 'option' element
        const option = document.createElement('option');
        // Set the '.innerHTML' property of the option element to the state's name
        option.innerHTML = state.name;
        // Set the '.value' property of the option element to the states abbreviation
        option.value = state.abbreviation;
        //Append the option element as a child of the select tag
        selectTag.appendChild(option);

      }
    }
  });

  window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const dataObject = Object.fromEntries(formData);
      const json = JSON.stringify(dataObject);
      //console.log(json);

      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        //rest form to its orginal state (clear it) so we know if something happened
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      }
    });
  });
