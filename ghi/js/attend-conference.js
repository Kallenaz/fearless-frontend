    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add("d-none");
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove("d-none");
    }



  //need an event listener for when the DOM loads
  window.addEventListener('DOMContentLoaded', async () => {
    //declare a variable that will hold the URL for the API view list states function that we just created
    const url = 'http://localhost:8001/api/attendees/';
    //fetch the URL with the await keyword so that we get the response, not the Promise
    const response = await fetch(url);
    //If the response is okay, get the data using the .json method and await
    if (response.ok) {
      const data = await response.json();
      //console.log(data);
      //Get the select tag element by its id 'state'
      const selectTag = document.getElementById('name');
      //For each state in the states property of the data
      for (let attendee of data.attendees) {
        //Create an 'option' element
        const option = document.createElement('option');
        // Set the '.innerHTML' property of the option element to the state's name
        option.innerHTML = attendee.email;
        // Set the '.value' property of the option element to the states abbreviation
        option.value = attendee.name;
        //Append the option element as a child of the select tag
        selectTag.appendChild(option);
      }
    }
  });


// Get the attendee form element by its id
// Add an event handler for the submit event
// Prevent the default from happening
// Create a FormData object from the form
// Get a new object from the form data's entries
// Create options for the fetch
// Make the fetch using the await keyword to the URL http://localhost:8001/api/attendees/

  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const dataObject = Object.fromEntries(formData);
    const json = JSON.stringify(dataObject);
    //console.log(json);

    const attendeeUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      //When the response from the fetch is good, you need to remove the d-none from the success alert and add d-none to the form. That code you just wrote that adds and removes the d-none class from the loading icon and the select tag? You do the same thing with the form, adding the d-none class to it. You look in the HTML and see that the alert has id="success-message". You use document.getElementById to get the success alert and use classList to remove the d-none class from it.
      const successAlert = document.getElementById('success-message');
      successAlert.classList.remove("d-none");
      formTag.classList.add("d-none");
      const newAttendee = await response.json();
      console.log(newAttendee);
      //reset form (clear it)
      formTag.reset();
    }
  });
