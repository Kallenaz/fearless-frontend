
//need an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare a variable that will hold the URL for the API view list conferences function that we just created
    const url = 'http://localhost:8000/api/locations/';
    //fetch the URL with the await keyword so that we get the response, not the Promise
    const response = await fetch(url);
    //If the response is okay, get the data using the .json method and await
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      //Get the select tag element by its id 'name'
      const locationSelect = document.getElementById('location');
      //For each conference location in the conference location property of the data
      for (let location of data.locations) {
        //Create an 'option' element
        const option = document.createElement('option');
        // Set the '.innerHTML' property of the option element to the conference location's name
        option.innerHTML = location.name;
        // Set the '.value' property of the option element to the location id
        option.value = location.id;
        //Append the option element as a child of the select tag
        locationSelect.appendChild(option);

      }
    }
  });

  window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const dataObject = Object.fromEntries(formData);
      const json = JSON.stringify(dataObject);
      //console.log(json);

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);
        //reset form to its orginal state (clear it) so we know if something happened
        formTag.reset();
      }
    });
  });
