import React, { useState, useEffect } from "react";

function AttendeesList(props) {
  const [attendees, setAttendees] = useState([]);

  async function fetchAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setAttendees(data.attendees);
    }
  }

  useEffect(() => {
    fetchAttendees();
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {/*this is just a for loop*/}
        {attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;
