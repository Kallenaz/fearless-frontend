//add react libraries, react and react-dom, css file, app function from app.js file
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

//root and root.rend is telling react where to take that fake html and put it
const root = ReactDOM.createRoot(document.getElementById('root'));
//the <App /> below is what runs the App function in the other file, whatever JSX the fxn returns gets put there, thats how it ends up bulding the HTML for the page
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  //check if resonse is ok
  if (response.ok) {
    //if it is, then get the data from teh responses json method
    const data = await response.json();
    //log the data to the console
    console.log(data);
    //below: attendees here is an attribute on the App.
    //this line of code sets a variable named attendees to the value inside the curly braces.
    //attendees becomes a property on the props parameter (props.attendees) that gets passed into the function in App.js. This is how we pass arguments to the React functions(components). In this case, we are passing it to the App component.
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  //if the data is not ok
  } else {
    //log the response as an error to the console
    console.error(response);
  }
}
loadAttendees();
