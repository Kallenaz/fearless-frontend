import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import {BrowserRouter, Route, Routes } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="attendees">
            <Route index element={<AttendeesList attendees={props.attendees} />}/>
            <Route path="new" element={<AttendConferenceForm />}/>
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />}/>
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />}/>
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;










// {/*<ConferenceForm />*/}
// {/*<PresentationForm />*/}
// {/*<AttendConferenceForm />*/}
// {/*<AttendeesList attendees={props.attendees} />*/}
//this is what the App component looked like before being changed: the attendees property on the props parameter ({props.attendees.length}) is coming from index.js, this is how we pass arguments to the React functions (components). App is our component.
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;
